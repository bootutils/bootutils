/* 
 * If linux/types.h is already been included, assume it has defined
 * everything we need.  (cross fingers)  Other header files may have 
 * also defined the types that we need.
 */

#ifndef _BLKID_TYPES_H
#define _BLKID_TYPES_H


#include <linux/types.h>
#include <sys/types.h>
#include <sys/stat.h>

#endif /* _BLKID_TYPES_H */

