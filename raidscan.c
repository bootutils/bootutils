/*
 * 	raidscan.c - Utility to initiate scanning of software RAID devices
 * 	Copyright (C) 2005-2009, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 
 */

#include "config.h"

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/major.h>
#include <linux/raid/md_u.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>


#define MD0 "/dev/md0"


int main() {
	int autodetect = 1;
	int res;
	int fd;
	
	struct stat tstat;

	
	printf("RaidScan v%s - Copyright (c) 2005-2009 Linux Based Systems Design\n\n",VERSION);

	/* Create device so we can ioctl() it */
	if (stat(MD0,&tstat)) {
		res = mknod(MD0, S_IFBLK | 0600, makedev(MD_MAJOR, 0));
		if (res) {
			fprintf(stderr,"ERROR creating device '%s': %s\n",MD0,strerror(errno));
			return 1;
		}
	}

	/* Open device */
	fd = open(MD0, O_RDWR);
	if (fd < 0) {
		fprintf(stderr,"ERROR opening device '%s': %s\n",MD0,strerror(errno));
		return 1;
	}

	/* Initiate RAID autodetection */
	res = ioctl(fd,RAID_AUTORUN,&autodetect);
	if (res) {
		fprintf(stderr,"ERROR sending ioctl() to device '%s': %s\n",MD0,strerror(errno));
		return 1;
	}

	close(fd);
}

