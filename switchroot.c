/*
 * 	switchroot.c - Utility to switch root on Linux 2.6 systems
 * 	Copyright (C) 2005-2009, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 
 * --
 * Some code taken from Redhat's nash.c
 *
 * Credits:
 * - Erik Troan (ewt@redhat.com)
 * - Jeremy Katz (katzj@redhat.com)
 * - Peter Jones (pjones@redhat.com)
 *
 * Copyright 2002-2005 Red Hat Software 
 * --
 *
 * Example /init:
 *
 * 	#!/bin/sh
 *
 * 	mount -o ro -t ext3 /dev/hda1 /sysroot
 * 	exec switchroot /sysroot
 *
 */


#include "config.h"
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#ifdef HAVE_GETOPT_LONG
#include <getopt.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
//#include <sys/mount.h>
#include <linux/fs.h>
#include <sys/stat.h>
#include "blkid.h"

/* This stuff isn't defined anywhere else? */
#ifndef MNT_FORCE
#define MNT_FORCE 0x1
#endif

#ifndef MNT_DETACH
#define MNT_DETACH 0x2
#endif


/* Be verbose? */
int verbose = 0;
int useEmergency = 0;


/* Possible init's */
const char *initprogs[] = { 
	"/sbin/init",
	"/sbin/initng",
	"/etc/init",
	"/bin/init",
	"/bin/sh",
	NULL 
};

/* Don't try to unmount the old "/", there's no way to do it. */
const char *umounts[] = {
	"/dev/pts",
	"/dev/shm",
	"/dev",
	"/proc",
	"/sys",
	NULL
};


/* Function to run emergency command if something fails */
void emergency() {
	if (useEmergency)
		execl("/emergency", "/emergency", NULL);
	exit(1);
}

#ifdef __powerpc__
#define CMDLINESIZE 256
#else
#define CMDLINESIZE 1024
#endif

/* Get the contents of the kernel command line from /proc/cmdline */
char *getKernelCmdLine() {
	int fd, i;
	char *buf;
	char *res = NULL;


	if ((fd = open("/proc/cmdline", O_RDONLY, 0)) < 0) {
		printf("switchroot: failed to open /proc/cmdline: %d\n", errno);
		return NULL;
	}

	if (!(buf = malloc(CMDLINESIZE)))
		return buf;

	if ((i = read(fd, buf, CMDLINESIZE)) < 0) {
		printf("switchroot: failed to read /proc/cmdline: %d\n", errno);
		close(fd);
		return NULL;
	}

	close(fd);

	/* Check if we didn't read anything, if not end */
	if (i == 0)
		goto end;
	
	/* Nuke last character to end our string */
	buf[i - 1] = '\0';

	/* Resize our result to the string length */
	res = strdup(buf);
	free(buf);

end:	
	return res;
}


/* Remove all files/directories below dirName -- don't cross mountpoints */
int recursiveRemove(char * dirName) {
	struct stat sb,rb;
	DIR * dir;
	struct dirent * d;
	char * strBuf = alloca(strlen(dirName) + 1024);


	if (!(dir = opendir(dirName))) {
		fprintf(stderr, "error opening %s: %d\n", dirName, errno);
		return 0;
	}

	if (fstat(dirfd(dir),&rb)) {
		fprintf(stderr, "unable to stat %s: %d\n", dirName, errno);
		return 0;
	}

	errno = 0;
	while ((d = readdir(dir))) {
		errno = 0;

		if (!strcmp(d->d_name, ".") || !strcmp(d->d_name, "..")) {
			errno = 0;
			continue;
		}

		strcpy(strBuf, dirName);
		strcat(strBuf, "/");
		strcat(strBuf, d->d_name);

		if (lstat(strBuf, &sb)) {
			fprintf(stderr, "failed to stat %s: %d\n", strBuf, errno);
			errno = 0;
			continue;
		}

		/* only descend into subdirectories if device is same as dir */
		if (S_ISDIR(sb.st_mode)) {
			if (sb.st_dev == rb.st_dev) {
				recursiveRemove(strBuf);
				if (rmdir(strBuf))
					fprintf(stderr, "failed to rmdir %s: %d\n", strBuf, errno);
			}
			errno = 0;
			continue;
		}

		if (unlink(strBuf)) {
			fprintf(stderr, "failed to remove %s: %d\n", strBuf, errno);
			errno = 0;
			continue;
		}
	}

	if (errno) {
		closedir(dir);
		printf("error reading from %s: %d\n", dirName, errno);
		return 1;
	}

	closedir(dir);

	return 0;
}


#define MAX_INIT_ARGS 32
/* 2.6 magic not-pivot-root but kind of similar stuff.
 * This is based on code from klibc/utils/run_init.c
 */
int switchroot(char *newRoot, char mountRoot, char *mountRootDev, char preserveDev, 
		char forceInit) {

	int umountStart = 0;
	/* Kernel commandline stuff */
	char *cmdline;
	char *init = NULL;
	/* Args to pass to init */
	int argc = 1;
	char **initargs;
	int forceSingle = 0;
	/* Misc */	
	char *c, *s;
	int fd, i;
    

	/* Grab kernel commandline */
	cmdline = getKernelCmdLine();

	/* Check if we actually have a commandline */
	if (cmdline) {
	
		/* Pull out tokens */	
		while (s = strtok(cmdline, " ")) {
			/* Lets see what we got ... */
			if (verbose) printf("switchroot: kernel cmdline option '%s'\n",s);

			/* Check if we have special case init=, this must be chopped out */
			if (!strncmp(s,"init=",5)) {
				init = s+5;
				if (verbose) printf("switchroot: init= specified on commandline '%s'\n",init);
			}

			/* Check if we have a root= command line option use it */
			if (!strncmp(s,"root=",5)) {
				mountRootDev = s+5;
				if (verbose) printf("switchroot: root= specified on commandline '%s'\n",init);
			}
		
			/* Check if we have special case 'single', this is to force single-user mode */
			if (!strncmp(s,"single",6)) {
				forceSingle = 1;
				if (verbose) printf("switchroot: Found 'single' on kernel commandline, forcing single-user mode\n");
			}

			/* 
			 * Use of strtok WILL break cmdline, so we make sure its NULL,
			 * here
			 */
			if (cmdline)
				cmdline = NULL;
		}
	}
	
	/* Check if we have a root= command line option use it */
	if (mountRoot) {
		char *rootSpec = mountRootDev;
		const char *rootDev;
		const char *fstype;


		/* Init cache */
		mount_blkid_get_cache();

		/* If still NULL, fallback to default */
		if (!rootSpec)
		{
default_spec:
			rootSpec = "LABEL:root";
		}

		/* Check if this is a label spec */
		if ((s = strstr(rootSpec,"LABEL:"))) {
				char *rootLabel = s + 6;


				/* Make sure we have a label */
				if (!*rootLabel) {
					printf("switchroot: Root label is blank, falling back to default\n");
					goto default_spec;
				}

				/* Get device name */
				if (!(rootDev = mount_get_devname_by_label(rootLabel))) {
					printf("switchroot: Failed to find filesystem with LABEL=%s\n",rootLabel);
					emergency();
				}
		} else {
			/* If not it is a device spec */
			rootDev = rootSpec;
		}
		
		/* Get primary type */
		if (!(fstype = do_guess_fstype(rootDev))) {
			printf("switchroot: Failed to filesystem type for %s\n",rootDev);
			emergency();
		}
	
		if (verbose) printf("Mounting root device %s[%s]...",rootDev,fstype);

		/* Symlink root device */
		if (!symlink(rootDev,"/dev/root"))
			rootDev = "/dev/root";
		else
			printf("switchroot: Failed to symlink root device %s to /dev/root: %s",rootDev,strerror(errno));
			
		/* See if we can mount */
		if (mount(rootDev,newRoot,fstype,MS_RDONLY,NULL)) {
			if (verbose) printf("FAILED\n");
			printf("switchroot: Failed to mount %s(%s) on %s: %s\n",rootDev,fstype,newRoot,
					strerror(errno));
			emergency();
		}
		if (verbose) printf("done\n");
	
		mount_blkid_put_cache();
	}

	/* Check if we can chdir to the new root */
	if (chdir(newRoot)) {
		printf("switchroot: chdir(%s) failed: %d\n", newRoot, errno);
		emergency();
	}

	/* Dup console to stdin/out/err */
	if ((fd = open("/dev/console", O_RDWR)) < 0) {
		printf("switchroot: ERROR opening /dev/console!!!!: %s\n", strerror(errno));
		fd = 0;
	}

	if (dup2(fd, STDIN_FILENO) != STDIN_FILENO) 
		printf("switchroot: error dup2'ing fd of 0 to STDIN: %s\n", strerror(errno));
	if (dup2(fd, STDOUT_FILENO) != STDOUT_FILENO) 
		printf("switchroot: error dup2'ing fd of 1 to STDOUT: %s\n", strerror(errno));
	if (dup2(fd, STDERR_FILENO) != STDERR_FILENO) 
		printf("switchroot: error dup2'ing fd of 2 to STDERR: %s\n", strerror(errno));
	if (fd > STDERR_FILENO)
        	close(fd);

	/* Check if we must move /dev aswell */
	if (preserveDev) {
		umountStart = 3;
		if (verbose) printf("Preserving devices...");
		mount("/dev", "./dev", NULL, MS_MOVE, NULL);
		if (verbose) printf("done\n");
	}

	/* Dispose of / */
	recursiveRemove("/");

	fd = open("/", O_RDONLY);
	for (i = umountStart; umounts[i] != NULL; i++) {
		if (verbose) printf("Unmounting %s...", umounts[i]);
		/* Try unmount nicely */
		if (umount2(umounts[i], MNT_DETACH)) {
			if (verbose) printf("forcing...", umounts[i]);
			umount2(umounts[i], MNT_FORCE);
		}
		if (verbose) printf("done\n");
	}

	/* Move */
	if (mount(".", "/", NULL, MS_MOVE, NULL)) {
		printf("switchroot: mount failed: %s\n", strerror(errno));
		close(fd);
		return 1;
	}

	/* Switch to new root */
	if (chroot(".") || chdir("/")) {
		printf("switchroot: chroot() failed: %s\n", strerror(errno));
		close(fd);
		return 1;
	}
    
	/* release the old "/" */
	close(fd);

	/* If there was no init specified on commandline, check the allowed 
	 * init progs and take first one */
	if (!init) {
		int j;
	
	
		for (j = 0; initprogs[j] != NULL; j++) {
			if (!access(initprogs[j], X_OK)) {
				init = strdup(initprogs[j]);
				if (verbose) printf("switchroot: Using default init '%s'\n",init);
				break;
			}
		}
	}

	/* Check if we have yet a usable init */
	if (!init) {
		printf("switchroot: Error, no usefull init found\n");
		close(fd);
		return 1;
	}
	
	/* Allocate init args struct so we can build a list of args
	 * to pass to init */
	initargs = (char **)malloc(sizeof(char *)*(MAX_INIT_ARGS+1));

	/* Check if we must force a --init as a command line param
	 * We need this if we start init in a funny way such that its
	 * PID is not 1
	 */
	if (forceInit)
		initargs[argc++] = strdup("--init");

	/* Check if we're forcing single-user mode,
	 * this MUST be the LAST arg */
	if (forceSingle)
		initargs[argc++] = strdup("1");

	/* set first arg, and terminate the array */
	initargs[0] = init;
	initargs[argc] = NULL;
   
	if (verbose) {
			int i;

			for (i = 0; i <= argc; i++) {
				printf("switchroot: initarg %i:'%s'\n",i,initargs[i]);
			}
	}

	/* Check one last time if we can infact exec our init program */
	if (access(initargs[0], X_OK)) {
   		printf("switchroot: WARNING can't access %s\n", initargs[0]);
		close(fd);
		return 1;
	}

	/* Exec init */	
	if (verbose) printf("Root initialized\n");
	execv(initargs[0], initargs);

	printf("switchroot: exec of init (%s) failed!!!: %d\n", initargs[0], errno);

	return 1;
}




// Print out our usage
void printUsage(char **argv) {
	printf("Usage: %s [options] <newroot>\n",argv[0]);
	printf("\n");
	printf("Options:\n");
#ifdef HAVE_GETOPT_LONG
	printf("    -e,  --enable-emergency       Execute /emergency if something goes wrong\n");
	printf("    -f,  --force-init             Append --init to the args passed to init\n");
	printf("    -m,  --mount-root             Mount root device as <newroot> ourselves\n");
	printf("    -p,  --preserve-dev           Preserve the /dev directory on initramfs\n");
	printf("    -r,  --root-dev=DEVICE_SPEC   Root device to use, implies -m\n");
	printf("    -v,  --verbose                Be verbose in what we do\n");
	printf("    -h,  --help                   Display this page\n\n");
#else
	printf("    -e                            Execute /emergency if something goes wrong\n");
	printf("    -f                            Append --init to the args passed to init\n");
	printf("    -m                            Mount root device as <newroot> ourselves\n");
	printf("    -p                            Preserve the /dev directory on initramfs\n");
	printf("    -r                            Root device to use, implies -m\n");
	printf("    -v                            Be verbose in what we do\n");
	printf("    -h                            Display this page\n\n");
#endif
	printf("Note: \n");
	printf("    1. Using --preserve-dev is a VERY BAD idea with udev (configured perms)\n");
	printf("    2. DEVICE_SPEC can either be /dev/xxx or LABEL:xxxx\n");
	printf("\n");
}


int main(int argc, char **argv) {
	/* New root */
	char *newRoot;
	/* Preserve /dev? */
	char preserveDev = 0;
	/* Append --init to init args */
	char forceInit = 0;
	/* Mounting of root options */
	char mountRoot = 0;
	char *rootDev = NULL;

#ifdef HAVE_GETOPT_LONG
	/* Our long options */
	struct option long_options[] = {
		{"enable-emergency",0,0,'e'},
		{"force-init",0,0,'f'},
		{"mount-root",0,0,'m'},
		{"preserve-dev",0,0,'p'},
		{"root-dev",1,0,'r'},
		{"verbose",0,0,'v'},
		{"help",0,0,'h'},
		{0,0,0,0}
	};
#endif

	printf("SwitchRoot v%s - Copyright (c) 2005-2009 Linux Based Systems Design\n\n",VERSION);

	/* Go straight to help if no params provided */
	if (argc == 1) {
		printUsage(argv);
		return 1;
	}

	/* Loop with options */
	while (1) {
#ifdef HAVE_GETOPT_LONG
		int option_index = 0;
#endif
		char c;

		/* Process */
#ifdef HAVE_GETOPT_LONG
		c = getopt_long(argc,argv,"efmr:vh",long_options,&option_index);
#else
		c = getopt(argc,argv,"efmpr:vh");
#endif
		if (c == -1)
			break;

		/* Check... */
		switch (c) {
			case 'e':
				useEmergency = 1;
				break;
			case 'f':
				forceInit = 1;
				break;
			case 'm':
				mountRoot = 1;
				break;
			case 'p':
				preserveDev = 1;
				break;
			case 'r':
				rootDev = optarg;
				break;
			case 'v':
				verbose = 1;
				break;
			case 'h':
				printUsage(argv);
				return 0;
			default:
				printUsage(argv);
				return 1;
		}
	}

		/* We sholuld have 1 extra option */
	if (optind == argc) {
		fprintf(stderr,"%s: Error, newroot not specified\n",argv[0]);
		printUsage(argv);
		emergency();
	}

	/* Grab our newroot */
	newRoot = argv[optind++];
	
	/* If we STILL have params left over, its bad */
	if (optind < argc) {
		while (optind < argc)
			fprintf(stderr,"%s: Invalid option -- %s\n",argv[0],argv[optind++]);
		emergency();
	}

	/* Check what our PID is, if its not 1 display a warning */
	if (getpid() != 1)
		fprintf(stderr,"%s: Warning, switchroot should be invoked using exec to get PID 1\n",argv[0]);
	
	/* switch our root */	
	switchroot(newRoot,mountRoot,rootDev,preserveDev,forceInit);
	
	emergency();
}

// vim:ts=4
