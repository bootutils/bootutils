/*
 * 	blkid.c - Interface to libblkid
 * 	Copyright (C) 2005-2009, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 
 *	Code ripped out of util-linux (mount_blkid.c).
 *
 */


#include <stdio.h>
#include "blkid.h"

blkid_cache blkid;

void mount_blkid_get_cache(void) {
	blkid_get_cache(&blkid, NULL);
}

void mount_blkid_put_cache(void) {
	blkid_put_cache(blkid);
}

const char *mount_get_volume_label_by_spec(const char *spec) {
	return blkid_get_tag_value(blkid, "LABEL", spec);
}

const char *mount_get_devname(const char *spec) {
	return blkid_get_devname(blkid, spec, 0);
}

const char *mount_get_devname_by_uuid(const char *uuid) {
	return blkid_get_devname(blkid, "UUID", uuid);
}

const char *mount_get_devname_by_label(const char *label) {
	return blkid_get_devname(blkid, "LABEL", label);
}

/* Also when no UUID= or LABEL= occur? No verbose? No warnings? */
const char *mount_get_devname_for_mounting(const char *spec) {
	return blkid_get_devname(blkid, spec, 0);
}

const char *do_guess_fstype(const char *device)
{
        return blkid_get_tag_value(blkid, "TYPE", device);
}


