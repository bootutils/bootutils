#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ(2.59)
AC_INIT(bootutils)
AM_INIT_AUTOMAKE(bootutils,devel)
AC_CONFIG_SRCDIR([switchroot.c])
AC_CONFIG_HEADER([config.h])

AC_GNU_SOURCE


AC_ARG_ENABLE([static],
	[  --enable-static           Build static versions of these utilities],
	[
		LDFLAGS="-static $LDFLAGS"
		CFLAGS="-Os $CFLAGS"
	])

AC_ARG_ENABLE([blkid-devmapper],
	[  --enable-blkid-devmapper  Build with device-mapper support],
	[
		if test "$enableval" = "no"
		then
			echo "Disabling device-mapper support"
			DEVMAPPER_LIBS=''
		else
			AC_DEFINE([HAVE_DEVMAPPER], [], [Build with device-mapper support])
			echo "Enabling device-mapper support"
			DEVMAPPER_LIBS='/lib/libdevmapper.a'
		fi
	],
	echo "Disabling device-mapper support by default"
)
AC_SUBST(DEVMAPPER_LIBS)


# Checks for programs.
AC_PROG_CC
AC_PROG_RANLIB

dnl
dnl Check to see if llseek() is declared in unistd.h.  On some libc's 
dnl it is, and on others it isn't..... Thank you glibc developers....
dnl
AC_CHECK_DECL(llseek,
		[AC_DEFINE(HAVE_LLSEEK_PROTOTYPE,1,[has llseek prototype])],
		,
		[#include <unistd.h>])
dnl
dnl Check to see if lseek64() is declared in unistd.h.  Glibc's header files
dnl are so convoluted that I can't tell whether it will always be defined,
dnl and if it isn't defined while lseek64 is defined in the library, 
dnl disaster will strike.  
dnl
AC_CHECK_DECL(lseek64,
		[AC_DEFINE(HAVE_LSEEK64_PROTOTYPE,1,[has llseek64 prototype])],
		,
                [#define _LARGEFILE_SOURCE
                 #define _LARGEFILE64_SOURCE
                 #include <unistd.h>])

# Checks for header files.
AC_HEADER_DIRENT
AC_HEADER_STDC
AC_CHECK_HEADERS([ fcntl.h getopt.h inttypes.h stdlib.h string.h linux/fd.h sys/disk.h sys/disklabel.h sys/queue.h sys/ioctl.h sys/time.h sys/types.h sys/mkdev.h unistd.h errno.h ])


# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_OFF_T
AC_CHECK_MEMBERS([struct stat.st_rdev])
AC_HEADER_TIME
AC_TYPE_UINT16_T
AC_TYPE_UINT32_T
AC_TYPE_UINT8_T

# Checks for library functions.
AC_FUNC_ALLOCA
AC_FUNC_CLOSEDIR_VOID
AC_PROG_GCC_TRADITIONAL
AC_FUNC_LSTAT
AC_FUNC_LSTAT_FOLLOWS_SLASHED_SYMLINK
AC_HEADER_MAJOR
AC_FUNC_MALLOC
AC_FUNC_MEMCMP
AC_FUNC_STAT
AC_CHECK_FUNCS([ dup2 memset rmdir strchr strdup strerror strndup strrchr strstr strtol strtoul strtoull uname fstat64 llseek llseek64 ])

# Use devmapper
AC_DEFINE([HAVE_DEVMAPPER])

AC_CONFIG_FILES([
		 Makefile
		 libblkid/Makefile
 ])

AC_OUTPUT
